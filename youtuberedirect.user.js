// ==UserScript==
// @name            Youtube Redirect
// @version         1.0
// @description     Automatically redirect to subscription feed from the YouTube front page.
// @include         /^https?:\/\/www\.youtube\.com\/$/
// @run-at          document-start
// ==/UserScript==

(function() {

    window.stop();
    window.location = window.location + "feed/subscriptions/u";
    
})();