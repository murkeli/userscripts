// ==UserScript==
// @name            Dropbox Redirect
// @version         1.0
// @description     Redirects Dropbox image links straight to the image file.
// @include         http*://www.dropbox.com/*
// @grant           none
// @run-at          document-start
// ==/UserScript==

(function() {

    var base = 'dl.dropboxusercontent.com/'

    var url = document.location;
    var match = /(https?:\/\/)(?:www\.)?dropbox.com\/(\w+\/[a-z0-9]+\/.+\.?(?:jpg|png|gif|webm|mp4))/i.exec(url);
    if (match) {
        document.location = match[1] + base + match[2];
    }

})();