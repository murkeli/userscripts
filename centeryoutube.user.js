// ==UserScript==
// @name            Center Youtube body
// @version         1.1
// @description     Center new YouTube layout body when applicable and apply some nice styles.
// @grant           none
// @include         http*://www.youtube.com/*
// @updateURL       https://bitbucket.org/murkeli/userscripts/raw/master/centeryoutube.user.js
// @downloadURL     https://bitbucket.org/murkeli/userscripts/raw/master/centeryoutube.user.js
// ==/UserScript==

(function() {

    var hideGuide = true;

    var fixBody = function() {
        var style = document.body.style;
        style.marginLeft = 'auto';
        style.marginRight = 'auto';
        var width = window.innerWidth * 0.98;
        style.width = width + 'px';
        var timer;
        window.addEventListener('resize', function() {
            clearTimeout(timer);
            timer = setTimeout(function() {
                width = window.innerWidth * 0.98;
                style.width = width + 'px';
            }, 100);
        }, false);
    };
    
    var fixGuide = function() {
        var el = document.getElementById("guide");
        if (!hideGuide) {
            el.setAttribute("style", "position:absolute");
        } else {
            el.parentNode.removeChild(el);
        }
    };
    
    var blackenBackground = function() {
        document.getElementById("watch7-video-container").setAttribute("style", "background-color:#111");
    };
    
    var whitenSidebar = function() {
        document.getElementById("watch7-sidebar").setAttribute("style", "background-color:#fff");
    };
    
    var fixDislikeBar = function() {
        var color = "#DE0000";
        var parent = document.getElementById("watch7-views-info");
        var divs = parent.getElementsByTagName("div");
        if (divs.length == 3) {
            divs[0].setAttribute("style", "height:4px");
            var style = divs[2].getAttribute("style");
            divs[2].setAttribute("style", style + "; background-color: " + color);
        }
    };
    
    var fixHeaderAndFooter = function() {
        var style = "width:1003px;margin-left:auto;margin-right:auto;";
        document.getElementById("yt-masthead-container").setAttribute("style", style);
        document.getElementById("footer-container").setAttribute("style", style);
    };

    fixBody();
    fixHeaderAndFooter();
    
    if (location.pathname == "/watch") {
        fixGuide();
        blackenBackground();
        whitenSidebar();
        fixDislikeBar();
    }
    
})();